create database ShoppingMall 
on
(
name='ShoppingMall',
filename='E:\SM\ShoppingMall.mdf '
)
log on
(
name='ShoppingMall_log',
filename='E:\SM\ShoppingMall_log.ldf '

)
use ShoppingMall 
go

--商场信息表
create table Store 
(
number int  identity(1,1),
StoreName nvarchar(50) not null,
StoreAdd nvarchar(50) not null,
Tr_owner nvarchar(50) not null ,
Phone nvarchar(30) not null,
I_card nvarchar(40) not null,

)
--会员客户信息表
create table Client  
(
C_Id int primary key identity(1,1),
ClientName nvarchar(10) not null,
C_card nvarchar(40) not null,
Birthdate nvarchar(10) not null,
Phone nvarchar(30) not null,
Address nvarchar(50) not null,
)

--会员卡信息表
create table VIP
(
Id int primary key not null identity(1,1) ,
VipCN nvarchar(20)   not null,
C_Id int Foreign key references Client(C_Id) not null ,
KKdete nvarchar(10) not null,
FK_unit nvarchar(20) not null,
TypeId  nvarchar(10)  not null
)

--会员卡类型编号
create table  VipType 
(
Id int foreign key references VIP(Id) not null,
TypeId  nvarchar(10) not null,
TypeName nvarchar(10) not null,
VipLv  nvarchar(10) not null,
Discount nvarchar(10) not null


)
--会员积分活动表
create table activity
(
Id int identity(1,1) foreign key references VIP(Id),
VipCN nvarchar(20)   not null,
integral int

)

--会员消费信息表
create table Consumption
(
Id int identity(1,1),
OrderNumber nvarchar(10)primary key not null,
ProductNumber nvarchar(10) not null,
ProductQuantity nvarchar(10) not null,
Price money not null ,
Discount nvarchar(10) not null
)

--会员消费总金额表
create table summary
(
OrderNumber nvarchar(10)foreign key references Consumption(OrderNumber) not null,
OrderMoney nvarchar(10) not null,
OrderDate nvarchar(10) not null
)

--商城会员停车场信息表
create table Car
(
CarId int identity(1,1) Foreign key references Client(C_Id) ,
VipCN nvarchar(20) not null,
PlateNumber nvarchar(20) not null,
Intotime  nvarchar(10) not null,
cometime  nvarchar(10) not null

)

select * from  Store 
--增加
insert into Store(StoreName,StoreAdd,Tr_owner,Phone,I_card)values
('万豪购物广场','贺州','熊大','0597-77889900','452402************'),
('万豪购物广场','贺州','熊大','0597-77889900','452402************'),
('乐万家购物广场','贺州','熊二','0597-55669900','452402************')

--改
Update Store set Tr_owner='光头强'where number='1'
Update Store set StoreAdd='南宁'where number='2'


--删除
delete from  Store where number='4'

